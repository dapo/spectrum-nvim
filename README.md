spectrum is a light neovim colorscheme, inspired by [gruvbox](github.com/morhetz/gruvbox) and the github colors.
Colors for [lightline](github.com/ichyny/lightline.vim) are included, as well as colors for different terminal emulators.

It aims to provide enough contrast and be easy for the eyes.
It makes use of bold and italic emphasis.
If you use the TUI version of neovim, make sure that your terminal emulator supports true colors and bold/italic fonts.

## Galery

![](./assets/rust.png)

![](./assets/python.png)

## Installation

Point your favorite package manger to the url [https://codeberg.org/dapo/spectrum-nvim.git](https://codeberg.org/dapo/spectrum-nvim.git)

Then, in the `.vimrc` add:
```
colorscheme spectrum
```
For lua configuration use:
```lua
vim.cmd('colorscheme spectrum')
```

Build with [colorizer](https://codeberg.org/dapo/colorizer)
